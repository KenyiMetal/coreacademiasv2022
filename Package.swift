// swift-tools-version:5.5
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "CoreAcademias",
    platforms: [
        .iOS(.v13)
    ],
    products: [
        .library(name: "ACWebService", targets: ["ACWebService"]),
        .library(name: "ACUtils", targets: ["ACUtils"]),
        .library(name: "ACImageDownloader", targets: ["ACImageDownloader"]),
    ],
    dependencies: [
    ],
    targets: [
        .target(name: "ACWebService", dependencies: []),
        .target(name: "ACUtils", dependencies: []),
        .target(name: "ACImageDownloader", dependencies: ["ACWebService"]),
//        .testTarget(
//            name: "CoreAcademiasTests",
//            dependencies: ["CoreAcademias"]),
    ]
)
