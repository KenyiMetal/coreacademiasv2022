//
//  AlertManager.swift
//  
//
//  Created by Kenyi Rodriguez on 17/02/22.
//

import UIKit

extension UIViewController {
    
    public func showAlertWithTitle(_ title: String, message: String, accept: String) {
    
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let acceptAction = UIAlertAction(title: accept, style: .default, handler: nil)
        alertController.addAction(acceptAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    public func showAutomaticAlertWithTitle(_ title: String, message: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        self.present(alertController, animated: true) {
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
}
