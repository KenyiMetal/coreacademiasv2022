//
//  DateManager.swift
//  
//
//  Created by Kenyi Rodriguez on 17/02/22.
//

import Foundation

extension String {
    
    public func toDateWithFormart(_ formart: String) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = formart
        return dateFormatter.date(from: self)
    }
}

extension Date {
    
    public func toStringWithFormat(_ format: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.locale = Locale(identifier: "es_pe")
        return dateFormatter.string(from: self)
    }
}
