//
//  ImageDownloader.swift
//  
//
//  Created by Kenyi Rodriguez on 17/02/22.
//

import UIKit
import ACWebService

public typealias DownloadImageCompletion = (_ image: UIImage?, _ urlOrigin: String) -> Void

extension String {
    
    public func downloadImageWithCompletion(_ success: @escaping DownloadImageCompletion) {
        
        let configuration = WebService.Configuration.init(isJson: false)
        
        WebService.Request(urlString: self, configuration: configuration).execute { response in
            
            DispatchQueue.global(qos: .background).async {
                let image = response.toImage
                
                DispatchQueue.main.async {
                    success(image, response.urlOrigin)
                }
            }
        }
    }
}


